//
//  ContentView.swift
//  BirdyNew 
//
//  Created by Juraj Đurčević on 13.12.2023..
//

import SwiftUI

struct ContentView: View {
    
    @State var content:String = ""
    
    @State var tweets: [Tweet] = [
        Tweet(username: "durcevic", content: "Cool cool cool", isfavorite: true),
        Tweet(username: "nedic", content: "Cool cool", isfavorite: false),
        Tweet(username: "benkovic", content: "Cool", isfavorite: true)
    ]
    
    @EnvironmentObject var tweetData: TweetData
    @EnvironmentObject var userData: UserData
    
    @State var isPresented: Bool = false
    
    @State var username: String = ""
    
    var body: some View {
        VStack{
            HStack {
                Image(systemName:"bird")
                    .resizable()
                    .frame(width:28,height:28)
                    .foregroundColor(.cyan)
                Text("Birdy")
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    .foregroundStyle(.cyan)
                
                Spacer()
                
                if username.isEmpty{
                    Button(action:{isPresented=true}) {
                        Text("Log in")
                    }
                }
                else{
                    Button(action:{username=""}) {
                        Text("Log out")
                    }
                }
            }
            
            List($tweetData.tweets){ tweet in
                TweetRow(tweet: tweet)
            }.listStyle(.plain)
            
            Spacer()
            
            if !username.isEmpty{
                HStack{
                TextField("Content",text: $content)
                Button(action:{
                    let tweet = Tweet(username: username, 
                                      content: content,
                                      isfavorite: false)
                    
                    tweetData.tweets.append(tweet)
                    userData.myTweetsIds.append(tweet.id)
                    
                    content=""
                    
                }){
                    Text("Tweet")
                        .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        .foregroundStyle(.blue)
                    Image(systemName:"bird.fill")
                        .foregroundColor(.cyan)
                }.disabled(content.isEmpty)
            }
        }
        }
        .padding()
        
        .sheet(isPresented: $isPresented) {
            LoginView(username: $username,
                      isPresented: $isPresented)
        }
    }
}

#Preview {
    ContentView()
        .environmentObject(TweetData())
        .environmentObject(UserData()) //mozda
}
