//
//  ProfileView.swift
//  BirdyNew
//
//  Created by Juraj Đurčević on 11.01.2024..
//

import SwiftUI

struct ProfileView: View {
    
    @EnvironmentObject var tweetData: TweetData
    @EnvironmentObject var userData: UserData
    
    var body: some View {
        VStack{
            
            HStack{
                Image(userData.imageName)
                    .resizable()
                    .frame(width:128,height:128)
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                    .padding()
                Text(userData.username)
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                Spacer()
            }
            
            Text("My Tweets:")
            List(Binding.constant(tweetData.getTweets(ind: userData.myTweetsIds))){ tweet in
                TweetRow(tweet: tweet)
            }.listStyle(.plain)
            
            Text("Liked Tweets:")
            List(Binding.constant(tweetData.getTweets(ind: userData.likedTweetsIds))){ tweet in
                TweetRow(tweet: tweet)
            }.listStyle(.plain)
            
            
        }
    }
}

#Preview {
    ProfileView()
        .environmentObject(UserData())
        .environmentObject(TweetData())
}
