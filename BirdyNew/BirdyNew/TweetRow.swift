//
//  TweetRow.swift
//  BirdyNew
//
//  Created by Juraj Đurčević on 19.12.2023..
//

import SwiftUI

struct TweetRow: View {
    
    @Binding var tweet: Tweet
    
    @EnvironmentObject var userData: UserData
    
    var body: some View {
        HStack{
            
            Spacer()
            Image("brb")
                .resizable()
                .frame(width:68,height:68)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            Spacer()
            
            VStack{
                Text(tweet.username)
                    .bold()
                Text(tweet.content)
                DatePicker(selection: .constant(tweet.date), label: {Text("") })
            }
            
            Spacer()
            
            Button(action: {
                
                tweet.isfavorite.toggle()
                
                if(tweet.isfavorite){
                    userData.likedTweetsIds.append(tweet.id)
                }
                else {
                    if let ind = userData.likedTweetsIds.firstIndex(of:tweet.id){
                    userData.likedTweetsIds.remove(at: ind)
                    }
                }
            } ){
                
                if(tweet.isfavorite){
                    Image(systemName: "heart.fill")
                        .foregroundColor(.red)
                }
                else{
                    Image(systemName: "heart")
                        .foregroundColor(.red)
                }
            }
            Spacer()
        }
    }
}

#Preview {
    TweetRow(tweet: Binding.constant(Tweet(username: "testUser", content: "testing", isfavorite: false)))
        .environmentObject(UserData())
}
