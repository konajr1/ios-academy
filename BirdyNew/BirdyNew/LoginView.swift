//
//  LoginView.swift
//  BirdyNew
//
//  Created by Juraj Đurčević on 19.12.2023..
//

import SwiftUI

struct LoginView: View {
    
    @Binding var username: String
    @Binding var isPresented: Bool
    
    var body: some View {
        VStack{
            Image(systemName:"bird.fill")
                .resizable()
                .frame(width:50,height:50)
                .foregroundColor(.cyan)
            
            Text("Birdy")
                .font(.title)
                .foregroundColor(.cyan)
                .fontWeight(.bold)
            
            TextField("Input your username...",text: $username)
                .fixedSize(horizontal: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
            
            Button(action:{
                isPresented=false
            }){
                Text("Log in")
                    .foregroundColor(.blue)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
            }
            .frame(width:70,height:30)
            .background(Color.gray.opacity(0.2))
            .cornerRadius(15.0)
            .disabled(username.isEmpty)
        }
    }
}

#Preview {
    LoginView(username: Binding.constant(""), isPresented: Binding.constant(true))
}
