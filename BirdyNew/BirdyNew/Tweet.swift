//
//  Tweet.swift
//  BirdyNew
//
//  Created by Juraj Đurčević on 11.01.2024..
//

import Foundation

struct Tweet: Identifiable{
    var id = UUID() .uuidString
    let username:String
    var content: String
    let date:Date = Date()
    var isfavorite: Bool
}
