//
//  TweetData.swift
//  BirdyNew
//
//  Created by Juraj Đurčević on 11.01.2024..
//

import Foundation


class TweetData: ObservableObject{
    @Published var tweets: [Tweet] = [
        Tweet(username: "durcevic", content: "Cool cool cool", isfavorite: false),
        Tweet(username: "nedic", content: "Cool cool", isfavorite: false),
        Tweet(username: "benkovic", content: "Cool", isfavorite: false)
    ]
    
    func getTweets(ind: [String]) -> [Tweet]{
        return tweets.filter { tweet in
            return ind.contains(tweet.id)
        }
    }
}
