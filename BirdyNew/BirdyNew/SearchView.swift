//
//  SearchView.swift
//  BirdyNew
//
//  Created by Juraj Đurčević on 11.01.2024..
//

import SwiftUI

struct SearchView: View {
    
    
    
    @EnvironmentObject var tweetData: TweetData
    
    @State var query = ""
    
   var foundedTweets: [Tweet] {
        if query.isEmpty{
            return tweetData.tweets
        }
        else{
            return tweetData.tweets.filter { tweet in
                return tweet.username.contains(query)
                // && tweet.content.contains(query)
            }
        }
    }
    
    var body: some View {
        VStack{
            TextField("Search tweets...", text: $query)
                .autocapitalization(.none)
                .padding()
            
            
            List(Binding.constant(foundedTweets)){ tweet in
                TweetRow(tweet: tweet)
            }
            .listStyle(.plain)
        }
    }
}

#Preview {
    SearchView()
        .environmentObject(TweetData())
        .environmentObject(UserData())
}
