//
//  UserData.swift
//  BirdyNew
//
//  Created by Juraj Đurčević on 11.01.2024..
//

import Foundation
import Combine

class UserData: ObservableObject{
    @Published var username = "Juraj_Durc"
    @Published var imageName = "jurajProfilna"
    @Published var likedTweetsIds : [String]  = []
    @Published var myTweetsIds : [String] = []
}
