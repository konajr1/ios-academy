//
//  BirdyNewApp.swift
//  BirdyNew
//
//  Created by Juraj Đurčević on 13.12.2023..
//

import SwiftUI

@main
struct BirdyNewApp: App {
    
    @StateObject var tweetData = TweetData()
    @StateObject var userData = UserData()
    
    var body: some Scene {
        WindowGroup {
            TabView {
                
                SearchView()
                    .tabItem{
                        Label("Search", systemImage: "magnifyingglass")
                    }
                
                ContentView()
                    .tabItem {
                        Label("Home", systemImage: "house.fill")
                    }
                
                
                ProfileView()
                    .tabItem{
                        Label("Profile", systemImage: "person")
                    }
                
            }
            .environmentObject(tweetData)
            .environmentObject(userData)
        }
    }
}
